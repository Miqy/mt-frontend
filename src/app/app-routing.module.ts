import { LoginGuard } from './permissions/login-guard';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './permissions/auth-guard';


const routes: Routes = [
  { path: 'login', component: LoginComponent,  canActivate:[LoginGuard] },
  { path: 'home', component: HomeComponent, data:{requiresLogin: true}, canActivate:[AuthGuard] },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
