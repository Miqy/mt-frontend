import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });
  
  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  onClickSubmit(formData) {
    this.loginService.login(formData["username"], formData["password"]).subscribe(
      (data) => {
        this.router.navigate(['/home'])
      },
      (error) => console.log(error)
    );
  }

  onSubmit(){
    const formData = this.loginForm.value
    this.loginService.login(formData["username"], formData["password"]).subscribe(
      (data) => {
        this.router.navigate(['/home'])
      },
      (error) => console.log(error)
    );
    console.log(this.loginForm.value)
  }

}
