import { LoginService } from '../services/login.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private loginService: LoginService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean | UrlTree>|Promise<boolean | UrlTree>|boolean | UrlTree {
    const requiresLogin = route.data.requiresLogin || false;
  
    return this.loginService.status().pipe(
      map((isLogged) => {
        if (!isLogged && requiresLogin){
          this.router.navigateByUrl('/login')
        }
        return isLogged
      })
    );
  }
}