import { LoginService } from '../services/login.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

@Injectable()
export class LoginGuard implements CanActivate {

  constructor(private loginService: LoginService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean | UrlTree>|Promise<boolean | UrlTree>|boolean | UrlTree {
  
    return this.loginService.status().pipe(
      map((isLogged) => {
        if (isLogged){
          this.router.navigateByUrl('/home')
        }
        return !isLogged
      })
    );
  }
}