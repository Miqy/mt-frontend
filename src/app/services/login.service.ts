import { Observable } from 'rxjs/internal/Observable';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router) { }

  login(username: string, password: string): Observable<any> {
    return this.http.post("/api/users/auth/login/", {"username": username, "password": password})
  }

  status() : Observable<boolean>{
    return this.http.get<boolean>("/api/users/auth/login_status", {observe: 'response'}).pipe(
      map((response) => {
        if (response.status == 200){ return response.body['authenticated'] }
        return false
      })
    )
    
  }
}
